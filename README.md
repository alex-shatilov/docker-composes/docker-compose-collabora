# Описание
Данный compose файл поднимает Collabora Online офисный пакет на основе LibreOffice с возможностью совместного редактирования, работает в связке с Nextcloud.

# Требования перед установкой

- Docker, Docker Compose (Docker Engine release 1.13.0+)
- Nextcloud (21.11.3.6+)

# Для использования

- Клонируем репозиторий
- Правим значения под себя если требуется
- Делаем в корневой папке репозитория 
```
docker-compose up -d
```
- После включаем модуль Collabora в Nextcloud и указываем адрес контейнера с Collabora
